package com.ds.lab4week.controllers;

import com.ds.lab4week.entities.Product;
import com.ds.lab4week.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductsController {
    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity<Product> post(@RequestBody Product product){
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.create(product));
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAll(){
        return ResponseEntity.ok(productService.readAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Product>> getById(@PathVariable UUID id){
        if(productService.readById(id).isPresent()){
            return ResponseEntity.ok(productService.readById(id));
        }
        return ResponseEntity.notFound().build();
    }
}
