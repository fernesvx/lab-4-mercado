package com.ds.lab4week.controllers;

import com.ds.lab4week.entities.Sale;
import com.ds.lab4week.services.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales")
public class SaleController {
    @Autowired
    private SaleService saleService;

    @GetMapping
    public ResponseEntity<List<Sale>> getAll(){
        List<Sale> sales = saleService.readAll();
        return ResponseEntity.ok(sales);
    }

    @PostMapping
    public ResponseEntity<Sale> post(@RequestBody Sale sale){
        Sale sale1 = saleService.create(sale);
        if(sale1 != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(sale1);
        }
        return ResponseEntity.notFound().build();
    }
}
