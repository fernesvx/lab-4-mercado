package com.ds.lab4week;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab4weekApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab4weekApplication.class, args);
	}

}
