package com.ds.lab4week.services;

import com.ds.lab4week.entities.Product;
import com.ds.lab4week.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public Product create(Product product){
        if(product.getPrice()<0){
            return null;
        }
        return productRepository.save(product);
    }

    public List<Product> readAll(){
        return productRepository.findAll();
    }

    public Optional<Product> readById(UUID id){
        if(productRepository.findById(id).isPresent()){
            return productRepository.findById(id);
        }
        return Optional.empty();
    }
}
