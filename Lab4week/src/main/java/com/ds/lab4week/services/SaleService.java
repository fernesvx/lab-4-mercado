package com.ds.lab4week.services;

import com.ds.lab4week.entities.Product;
import com.ds.lab4week.entities.Sale;
import com.ds.lab4week.repositories.ProductRepository;
import com.ds.lab4week.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SaleService {
    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private ProductRepository productRepository;

    public List<Sale> readAll(){
        return saleRepository.findAll();
    }

    public Sale create(Sale sale){
        Optional<Product> product = productRepository.findById(sale.getProduct().getId());

        if(product.isEmpty()){
            return null;
        }
        if(sale.getUnits()>0){
            return null;
        }
        if(product.get().getStock() - sale.getUnits() < 0){
            return null;
        }

        product.get().setStock(product.get().getStock() - sale.getUnits());
        productRepository.save(product.get());

        double paymentValue = product.get().getPrice() * sale.getUnits();

        if(sale.getUnits() >= 10){
            if(sale.getUnits() >= 20){
                paymentValue *= 0.9;
            }
            else{
                paymentValue *= 0.95;
            }
        }

        sale.setPayment(paymentValue);

        return saleRepository.save(sale);
    }
}
