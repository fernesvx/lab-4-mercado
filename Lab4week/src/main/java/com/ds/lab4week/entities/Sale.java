package com.ds.lab4week.entities;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "SALE")
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID uuid;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;

    @Column(name = "UNITS")
    private int units;

    @Column(name = "DATE")
    private LocalDateTime date;

    @Column(name = "PAYMENT")
    private double payment;

    public Sale(){
        date = LocalDateTime.now();
    }

    public UUID getUuid() {
        return uuid;
    }

    public Product getProduct() {
        return product;
    }

    public int getUnits() {
        return units;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public double getPayment() {
        return payment;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setPayment(double payment) {
        this.payment = payment;
    }
}
